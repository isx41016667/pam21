# PAM Server
## @edt ASIX M06-ASO 2021-2022
### Servidor PAM (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **isx41016667/pam21:pam_time.so** Imatge PAM practica de permissos de temps de canviar el chfn.

```
entrar dind del servidor PAM
docker run --rm --name pam.edt.org -h pam.edt.org --net hisix2 -it cristiancondolo21/pam21:base

per entra com usuaris qualsevol:
docker exec -it pam.edt.org /bin/bash
su - unix01
``` 
